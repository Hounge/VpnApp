package com.white.vpnapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SessionData
{
  private SharedPreferences.Editor editor;
  private SharedPreferences pref;

  public SessionData(Context paramContext, String paramString)
  {
    this.pref = paramContext.getSharedPreferences(paramString, 0);
    this.editor = this.pref.edit();
  }

  public void clearSession()
  {
    this.editor.clear();
    this.editor.commit();
  }

  public boolean getBoolean(String paramString)
  {
    return this.pref.getBoolean(paramString, false);
  }

  public double getDouble(String paramString)
  {
    return Double.longBitsToDouble(this.pref.getLong(paramString, Double.doubleToLongBits(0.0D)));
  }

  public float getFloat(String paramString)
  {
    return this.pref.getFloat(paramString, 0.0F);
  }

  public int getInt(String paramString)
  {
    return this.pref.getInt(paramString, 0);
  }

  public double getLong(String paramString)
  {
    return this.pref.getLong(paramString, 0L);
  }

  public String getString(String paramString)
  {
    return this.pref.getString(paramString, null);
  }

  public void removeKey(String paramString)
  {
    this.editor.remove(paramString);
    this.editor.commit();
  }

  public void setBoolean(String paramString, boolean paramBoolean)
  {
    this.editor.putBoolean(paramString, paramBoolean);
    this.editor.commit();
  }

  public void setDouble(String paramString, double paramDouble)
  {
    this.editor.putLong(paramString, Double.doubleToLongBits(paramDouble));
    this.editor.commit();
  }

  public void setFloat(String paramString, float paramFloat)
  {
    this.editor.putFloat(paramString, paramFloat);
    this.editor.commit();
  }

  public void setInt(String paramString, int paramInt)
  {
    this.editor.putInt(paramString, paramInt);
    this.editor.commit();
  }

  public void setLong(String paramString, long paramLong)
  {
    this.editor.putLong(paramString, Double.doubleToLongBits(paramLong));
    this.editor.commit();
  }

  public void setString(String paramString1, String paramString2)
  {
    this.editor.putString(paramString1, paramString2);
    this.editor.commit();
  }
}

/* Location:           C:\apktool\classes_dex2jar.jar
 * Qualified Name:     com.vijaywebsolutions.vpn_app.SessionData
 * JD-Core Version:    0.6.0
 */