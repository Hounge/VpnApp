package com.white.vpnapp;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;
import android.os.Build;

public class Subscribe extends Activity  implements MyInterface
{
	  Context context;
	  ImageView iv_Back;
	  TextView tv_Title;
	  WebView webView;

	  private void init_var()
	  {
	    this.context = this;
	    this.tv_Title = ((TextView)findViewById(R.id.tv_Title));
	    this.tv_Title.setText(this.context.getResources().getString(R.string.RenewOrSubscribe));
	    this.iv_Back = ((ImageView)findViewById(R.id.iv_Back));
	    this.iv_Back.setOnClickListener(new View.OnClickListener()
	    {
	      public void onClick(View paramView)
	      {
	        Subscribe.this.finish();
	        Subscribe.this.onBackPressed();
	      }
	    });
	    this.webView = ((WebView)findViewById(2131296280));
	    this.webView.loadUrl("http://74.54.90.136/auth_net/authorize_net.php?username=" + (String)MainActivity.hashMap_ServerDetails.get("Username") + "&account=" + (String)MainActivity.hashMap_ServerDetails.get("Password"));
	  }

	  protected void onCreate(Bundle paramBundle)
	  {
	    super.onCreate(paramBundle);
	    setContentView(2130903045);
	    init_var();
	  }

	  public void printLog(String paramString)
	  {
	    Log.d("sss", paramString);
	  }
	}