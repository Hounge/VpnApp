package com.white.vpnapp;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.HttpHostConnectException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONParser {
	private String WEBSERVICE_EXT = "/format/json";
	private String WEBSERVICE_MIDDLE = "/index.php/";
	private String WEBSERVICE_URL;
	private InputStream is = null;
	private JSONObject jObj = null;
	private String json = "";

	public JSONObject getJsonObjectFromUrl(String paramString) {
		try {
			this.is = new DefaultHttpClient()
					.execute(new HttpPost(paramString)).getEntity()
					.getContent();
			BufferedReader localBufferedReader = new BufferedReader(
					new InputStreamReader(this.is, "iso-8859-1"), 8);
			StringBuilder localStringBuilder = new StringBuilder();
			while (true) {
				String str = localBufferedReader.readLine();
				if (str == null) {
					this.is.close();
					this.json = localStringBuilder.toString();
					this.jObj = new JSONObject(this.json);
					return this.jObj;
				}
				localStringBuilder.append(str + "\n");
			}
		} catch (UnsupportedEncodingException localUnsupportedEncodingException) {
			while (true)
				localUnsupportedEncodingException.printStackTrace();
		} catch (ClientProtocolException localClientProtocolException) {
			while (true)
				localClientProtocolException.printStackTrace();
		} catch (IOException localIOException) {
			while (true)
				localIOException.printStackTrace();
		} catch (JSONException localJSONException) {
			while (true)
				Log.e("JSON Parser",
						"Error parsing data " + localJSONException.toString());
		} catch (Exception localException) {
			while (true)
				localException.printStackTrace();
		}
	}

	public JSONObject getJsonObjectFromWebservice(String paramString) {
		try {
			System.out.println(this.WEBSERVICE_URL + this.WEBSERVICE_MIDDLE
					+ paramString + this.WEBSERVICE_EXT);
			this.is = new DefaultHttpClient()
					.execute(
							new HttpPost(this.WEBSERVICE_URL
									+ this.WEBSERVICE_MIDDLE + paramString
									+ this.WEBSERVICE_EXT)).getEntity()
					.getContent();
			BufferedReader localBufferedReader = new BufferedReader(
					new InputStreamReader(this.is, "iso-8859-1"), 8);
			StringBuilder localStringBuilder = new StringBuilder();
			while (true) {
				String str = localBufferedReader.readLine();
				if (str == null) {
					this.is.close();
					this.json = localStringBuilder.toString();
					this.jObj = new JSONObject(this.json);
					return this.jObj;
				}
				localStringBuilder.append(str + "\n");
			}
		} catch (UnsupportedEncodingException localUnsupportedEncodingException) {
			while (true)
				localUnsupportedEncodingException.printStackTrace();
		} catch (ClientProtocolException localClientProtocolException) {
			while (true)
				localClientProtocolException.printStackTrace();
		} catch (IOException localIOException) {
			while (true)
				localIOException.printStackTrace();
		} catch (JSONException localJSONException) {
			while (true)
				Log.e("JSON Parser",
						"Error parsing data " + localJSONException.toString());
		} catch (Exception localException) {
			while (true)
				localException.printStackTrace();
		}
	}

	public JSONObject getJsonObjectFromWebservice(String paramString,
			HashMap<String, String> paramHashMap) {
		try {
			DefaultHttpClient localDefaultHttpClient = new DefaultHttpClient();
			HttpPost localHttpPost = new HttpPost(paramString);

			ArrayList<BasicNameValuePair> localArrayList = new ArrayList();
			Iterator localIterator1 = paramHashMap.entrySet().iterator();
			while (localIterator1.hasNext()) {
				Map.Entry<String, String> localEntry = (Map.Entry) localIterator1
						.next();
				BasicNameValuePair localBasicNameValuePair2 = new BasicNameValuePair(
						localEntry.getKey(), localEntry.getValue());
				localArrayList.add(localBasicNameValuePair2);
			}

			UrlEncodedFormEntity localUrlEncodedFormEntity = new UrlEncodedFormEntity(
					localArrayList);
			localHttpPost.setEntity(localUrlEncodedFormEntity);
			this.is = localDefaultHttpClient.execute(localHttpPost).getEntity()
					.getContent();
			BufferedReader localBufferedReader = new BufferedReader(
					new InputStreamReader(this.is, "iso-8859-1"), 8);

			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = localBufferedReader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			json = sb.toString();
			Log.e("JSON ####", "" + json);
			jObj = new JSONObject(json);
			Log.e("JSONObject", "" + jObj);
			return jObj;

		} catch (HttpHostConnectException localHttpHostConnectException) {
			localHttpHostConnectException.printStackTrace();
			return null;
		} catch (Exception localException) {

			localException.printStackTrace();
			return null;

		}
	}
}

/*
 * Location: C:\apktool\classes_dex2jar.jar Qualified Name:
 * com.vijaywebsolutions.services.JSONParser JD-Core Version: 0.6.0
 */