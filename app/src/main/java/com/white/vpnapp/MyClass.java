package com.white.vpnapp;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MyClass {
	private Context _context;
	ProgressDialog progressDialog;

	public MyClass(Context paramContext) {
		this._context = paramContext;
	}

	public float convertDpToPixel(float paramFloat) {
		return paramFloat
				* (this._context.getResources().getDisplayMetrics().densityDpi / 160.0F);
	}

	public void dismissProgressDialog() {
		this.progressDialog.dismiss();
	}

	@SuppressLint({ "SimpleDateFormat" })
	public long getDateDiffString(String paramString1, String paramString2) {
		Date localDate1 = null;
		SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat(
				"yyyy-MM-dd");
		try {
			localDate1 = localSimpleDateFormat.parse(paramString2);
			Date localDate2 = localSimpleDateFormat.parse(paramString1);
			long l = localDate1.getTime();
			return (localDate2.getTime() - l) / 86400000L;
		} catch (ParseException localParseException) {
			localParseException.printStackTrace();
			return -1;
		}
	}

	public boolean isInternetAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) this._context
				.getSystemService("connectivity");

		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}

	@SuppressLint({ "SimpleDateFormat" })
	public boolean isPastDate(String paramString) {
		SimpleDateFormat localSimpleDateFormat = new SimpleDateFormat(
				"MM-dd-yyyy");
		try {
			Calendar localCalendar = Calendar.getInstance();
			Log.d("sss",
					"Today:"
							+ localSimpleDateFormat.format(localCalendar.getTime()));
			long l = getDateDiffString(paramString,
					localSimpleDateFormat.format(localCalendar.getTime()));
			boolean bool = l < 0L;
			return bool;
		} catch (Exception localException) {
			localException.printStackTrace();
		}
		return false;
	}

	public void setErrorMessage(EditText paramEditText, String paramString) {
		paramEditText.setError(Html.fromHtml("<font color='#FF0000'>"
				+ paramString + "</font>"));
		paramEditText.requestFocus();
	}

	public void showProgressDialog() {
		this.progressDialog = new ProgressDialog(new ContextThemeWrapper(
				this._context, 2131165184));
		this.progressDialog.setMessage("Loading");
		this.progressDialog.setProgressStyle(0);
		this.progressDialog.setCancelable(false);
		this.progressDialog.show();
	}

	public void showToast(String paramString) {
		if (!paramString.equalsIgnoreCase("")) {
			View localView = ((LayoutInflater) this._context
					.getSystemService("layout_inflater")).inflate(2130903048,
					null);
			((TextView) localView.findViewById(2131296287))
					.setText(paramString);
			Toast localToast = new Toast(this._context);
			localToast.setGravity(17, 0, 0);
			localToast.setDuration(1);
			localToast.setView(localView);
			localToast.show();
		}
	}
}

/*
 * Location: C:\apktool\classes_dex2jar.jar Qualified Name:
 * com.vijaywebsolutions.services.MyClass JD-Core Version: 0.6.0
 */