package com.white.vpnapp;


import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.os.Build;

public class RegisterActivity extends Activity implements View.OnClickListener, MyInterface
	{
	
	  Context context;
	  EditText[] et_array = new EditText[4];
	  HashMap<String, String> hashMap;
	  MyClass myClass;
	  String[] str_Array = new String[4];
	  TextView tv_Title;

	  private void fill_String_Array()
	  {
	    for (int i = 0; ; i++)
	    {
	      if (i >= this.et_array.length)
	        return;
	      this.str_Array[i] = this.et_array[i].getText().toString().trim();
	    }
	  }

	  private void init_var()
	  {
	    this.context = this;
	    this.myClass = new MyClass(this.context);
	    this.tv_Title = ((TextView)findViewById(R.id.tv_Title));
	    this.tv_Title.setText(this.context.getResources().getString(R.string.Register));
	    this.et_array[0] = ((EditText)findViewById(R.id.et_User_Name));
	    this.et_array[1] = ((EditText)findViewById(R.id.et_Email_Id));
	    this.et_array[2] = ((EditText)findViewById(R.id.et_Password));
	    this.et_array[3] = ((EditText)findViewById(R.id.et_Confirm_Password));
	  }

	  private void reset_data()
	  {
	    for (int i = 0; ; i++)
	    {
	      if (i >= this.et_array.length)
	        return;
	      this.et_array[i].setText("");
	    }
	  }

	  public void onBackPressed()
	  {
	    super.onBackPressed();
	    finish();
	  }

	  public void onClick(View paramView)
	  {
	    switch (paramView.getId())
	    {
	    default:
	      return;
	    case R.id.iv_Back:
	      onBackPressed();
	      break;
	    case R.id.btn_Register:
	    	
	    	fill_String_Array();
	    	if (this.str_Array[0].length() == 0)
	    	{
	    		this.myClass.setErrorMessage(this.et_array[0], this.context.getResources().getString(R.string.Please_enter_username));
	    		return;
	    	}
	    	if (this.str_Array[1].length() == 0)
	    	{
	    		this.myClass.setErrorMessage(this.et_array[1], this.context.getResources().getString(R.string.Please_enter_Email));
	    		return;
	    	}
	    	if (this.str_Array[2].length() == 0)
	    	{
	    		this.myClass.setErrorMessage(this.et_array[2], this.context.getResources().getString(R.string.Please_enter_password));
	    		return;
	    	}
	    	if (this.str_Array[3].length() == 0)
	    	{
	    		this.myClass.setErrorMessage(this.et_array[3], this.context.getResources().getString(R.string.Please_enter_confirm_password));
	    		return;
	    	}
	    	if (!this.str_Array[2].equalsIgnoreCase(this.str_Array[3]))
	    	{
	    		this.myClass.setErrorMessage(this.et_array[3], this.context.getResources().getString(R.string.Password_and_confirm_password_must_be_same));
	    		return;
	    	}
	    	this.hashMap = new HashMap();
	    	this.hashMap.put("Username", this.str_Array[0]);
	    	this.hashMap.put("EmailAddress", this.str_Array[1]);
	    	this.hashMap.put("Password", this.str_Array[2]);
	    	new AsyncTaskUtility(this.context, this.hashMap, "http://74.54.90.136/login_webservice/registration.php").getJSONResponse(new AsyncTaskInterface()
	    	{
	    		public void getJSONObjectFromAsynTask(JSONObject paramJSONObject)
	    		{
	    			reset_data();
	    			try
	    			{
	    				if (paramJSONObject.getString("msg").equalsIgnoreCase("Registration Successful!"))
	    				{
	    					finish();
	    					startActivity(new Intent(context, LoginActivity.class));
	    				}
	    				Log.d("sss", "JSON: " + paramJSONObject);
	    			}
	    			catch (JSONException localJSONException)
	    			{
    					localJSONException.printStackTrace();
	    			}
	    		}
	    	});
	    	break;
	    }
	  }

	  protected void onCreate(Bundle paramBundle)
	  {
	    super.onCreate(paramBundle);
	    setContentView(R.layout.activity_register);
	    init_var();
	  }

	  public void printLog(String paramString)
	  {
	    Log.d("sss", paramString);
	  }


}
