package com.white.vpnapp;

import org.json.JSONObject;

public abstract class AsyncTaskInterface
{
  public abstract void getJSONObjectFromAsynTask(JSONObject paramJSONObject);
}

/* Location:           C:\apktool\classes_dex2jar.jar
 * Qualified Name:     com.vijaywebsolutions.services.AsyncTaskInterface
 * JD-Core Version:    0.6.0
 */