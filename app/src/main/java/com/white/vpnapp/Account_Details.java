package com.white.vpnapp;


import java.util.HashMap;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.os.Build;

public class Account_Details extends Activity implements View.OnClickListener, MyInterface
{
	  Context context;
	  HashMap<String, String> hashMap_ServerDetails;
	  TextView tv_Connection;
	  TextView tv_IP_Address;
	  TextView tv_Password;
	  TextView tv_Proxy_Server;
	  TextView tv_Subscription;
	  TextView tv_Title;
	  TextView tv_User_Name;

	  private void init_var()
	  {
	    this.context = this;
	    System.out.println("----------------------------------------------kkkkkkkkkkkkk");
	    this.tv_Title = ((TextView)findViewById(2131296290));
	    this.tv_Title.setText(this.context.getResources().getString(2131099671));
	    this.tv_Subscription = ((TextView)findViewById(2131296260));
	    this.tv_User_Name = ((TextView)findViewById(2131296261));
	    this.tv_Password = ((TextView)findViewById(2131296262));
	    this.tv_Proxy_Server = ((TextView)findViewById(2131296263));
	    this.tv_IP_Address = ((TextView)findViewById(2131296264));
	    this.tv_Connection = ((TextView)findViewById(2131296265));
	    this.hashMap_ServerDetails = MainActivity.hashMap_ServerDetails;
	    setData(this.hashMap_ServerDetails);
	  }

	  private void setData(HashMap<String, String> paramHashMap)
	  {
	    this.tv_Subscription.setText((CharSequence)paramHashMap.get("Trial"));
	    this.tv_User_Name.setText((CharSequence)paramHashMap.get("Username"));
	    this.tv_Password.setText((CharSequence)paramHashMap.get("Password"));
	    this.tv_Proxy_Server.setText((CharSequence)paramHashMap.get("Proxy"));
	    this.tv_IP_Address.setText((CharSequence)paramHashMap.get("IPAddress"));
	    this.tv_Connection.setText((CharSequence)paramHashMap.get("Trial"));
	  }

	  public void onBackPressed()
	  {
	    super.onBackPressed();
	    finish();
	  }

	  public void onClick(View paramView)
	  {
	    switch (paramView.getId())
	    {
	    default:
	      return;
	    case 2131296288:
	    }
	    onBackPressed();
	  }

	  protected void onCreate(Bundle paramBundle)
	  {
	    super.onCreate(paramBundle);
	    setContentView(2130903041);
	    init_var();
	  }

	  public void printLog(String paramString)
	  {
	  }
	}