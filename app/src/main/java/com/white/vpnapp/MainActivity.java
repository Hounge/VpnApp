package com.white.vpnapp;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.channels.DatagramChannel;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.http.conn.util.InetAddressUtils;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.VpnService;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends Activity implements View.OnClickListener,
		MyInterface {

	public static HashMap<String, String> hashMap_ServerDetails;
	public static String ipaddress;
	public static String mServerAddress = "70.85.247.145";
	public static int mServerPort = 1723;
	public Button btn_Connect;
	Context context;
	CountDownTimer countDownTimer;
	public DatagramChannel mTunnel = null;
	Dialog myDialog;
	RelativeLayout rLayout_AboutUs;
	RelativeLayout rLayout_DemoVPNSetup;
	RelativeLayout rLayout_MyAccount;
	RelativeLayout rLayout_RemainingData;
	RelativeLayout rLayout_Renew_Or_Subscribe;
	RelativeLayout rLayout_VPNAutoSetup;
	private SessionData sessionData;
	TextView tv_Title;

	private void init_var() {
		this.context = this;
		this.sessionData = new SessionData(this.context, "com.white.vpnapp");
		this.countDownTimer = new CountDownTimer(100000L, 100L) {
			public void onFinish() {
			}

			public void onTick(long paramLong) {
				countDownTimer.cancel();
			}
		};
		this.tv_Title = ((TextView) findViewById(R.id.tv_Title));
		this.tv_Title.setText(this.context.getResources().getString(
				R.string.Menu));
		((ImageView) findViewById(R.id.iv_Log_Out)).setVisibility(View.VISIBLE);
		this.btn_Connect = ((Button) findViewById(R.id.btn_Connect));
		this.btn_Connect.setOnClickListener(this);
		if (isMyServiceRunning(VpnService.class, this.context))
			System.out.println("Service Running");
		else
			System.out.println("Service not Running");

		this.rLayout_MyAccount = ((RelativeLayout) findViewById(R.id.rLayout_MyAccount));
		this.rLayout_VPNAutoSetup = ((RelativeLayout) findViewById(R.id.rLayout_VPNAutoSetup));
		this.rLayout_RemainingData = ((RelativeLayout) findViewById(R.id.rLayout_RemainingData));
		this.rLayout_DemoVPNSetup = ((RelativeLayout) findViewById(R.id.rLayout_DemoVPNSetup));
		this.rLayout_Renew_Or_Subscribe = ((RelativeLayout) findViewById(R.id.rLayout_Renew_Or_Subscribe));
		this.rLayout_AboutUs = ((RelativeLayout) findViewById(R.id.rLayout_AboutUs));
		this.rLayout_MyAccount.setOnClickListener(this);
		this.rLayout_VPNAutoSetup.setOnClickListener(this);
		this.rLayout_RemainingData.setOnClickListener(this);
		this.rLayout_DemoVPNSetup.setOnClickListener(this);
		this.rLayout_Renew_Or_Subscribe.setOnClickListener(this);
		this.rLayout_AboutUs.setOnClickListener(this);
	}

	public static boolean isMyServiceRunning(Class<?> paramClass,
			Context paramContext) {
		Iterator localIterator = ((ActivityManager) paramContext
				.getSystemService("activity")).getRunningServices(2147483647)
				.iterator();
		ActivityManager.RunningServiceInfo localRunningServiceInfo;
		do {
			if (!localIterator.hasNext())
				return false;

			localRunningServiceInfo = (ActivityManager.RunningServiceInfo) localIterator
					.next();
			if (paramClass.getName().equals(
					localRunningServiceInfo.service.getClassName()))
				return true;
		} while (localIterator.hasNext());

		return false;

	}

	private boolean run(InetSocketAddress paramInetSocketAddress)
			throws Exception {
		this.mTunnel = DatagramChannel.open();
		this.mTunnel.socket();
		this.mTunnel.disconnect();
		return true;
	}

	

	public void onClick(View paramView) {
		switch (paramView.getId()) {
		default:
			break;
		case R.id.iv_Back:
			onBackPressed();
			break;
		case R.id.iv_Log_Out:
			sessionData.clearSession();
			onBackPressed();
			break;
		case R.id.rLayout_MyAccount:
			startActivity(new Intent(this.context, Account_Details.class));
			break;
		case R.id.rLayout_AboutUs:
			startActivity(new Intent(this.context, AboutUsActivity.class));
			break;
		case R.id.rLayout_Renew_Or_Subscribe:
			startActivity(new Intent(this.context, Subscribe.class));
			break;
		case R.id.btn_Connect:
			if (btn_Connect.getText().toString().trim().contains("Disconnect")) {
				// TODO code disconnect
				
				Intent intent = new Intent(this, MyVpnService.class);
				stopService(intent);
				btn_Connect.setText("Connect");
			} else {
				// TODO code connect
				Intent intent = VpnService.prepare(getApplicationContext());
				if (intent != null) {
					startActivityForResult(intent, 101);
				} else {
					onActivityResult(0, RESULT_OK, null);
				}
//				btn_Connect.setText("Disconnect");
			}
			break;
		}

	}

	protected void onCreate(Bundle paramBundle) {
		super.onCreate(paramBundle);
		setContentView(R.layout.activity_main);
		init_var();
		String str = this.sessionData.getString("strhashmap");
		try {
			JSONObject localJSONObject = new JSONObject(str);
			hashMap_ServerDetails = new HashMap();
			hashMap_ServerDetails.put("Proxy",
					localJSONObject.getString("Proxy"));
			hashMap_ServerDetails
					.put("Paid", localJSONObject.getString("Paid"));
			hashMap_ServerDetails.put("Trial",
					localJSONObject.getString("Trial"));
			hashMap_ServerDetails.put("IPAddress",
					localJSONObject.getString("IPAddress"));
			hashMap_ServerDetails.put("AccountNumber",
					localJSONObject.getString("AccountNumber"));
			hashMap_ServerDetails.put("Password",
					localJSONObject.getString("Password"));
			hashMap_ServerDetails.put("Username",
					localJSONObject.getString("Username"));
			hashMap_ServerDetails.put("ProxyPort",
					localJSONObject.getString("ProxyPort"));

		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * if (str != null) { TypeToken<String> local1 = new TypeToken() { };
		 * hashMap_ServerDetails = (HashMap)new Gson().fromJson(str,
		 * local1.getType()); }
		 * 
		 * while (true) { System.out.println("hashMap_ServerDetails: " +
		 * hashMap_ServerDetails); mServerAddress =
		 * (String)hashMap_ServerDetails.get("Proxy"); if
		 * (OrbotVpnService.mTunnel != null)
		 * this.btn_Connect.setText(2131099687); try { label130: Enumeration
		 * localEnumeration1 = NetworkInterface.getNetworkInterfaces(); while
		 * (true) { boolean bool = localEnumeration1.hasMoreElements(); if
		 * (!bool) { return; hashMap_ServerDetails = null; break;
		 * this.btn_Connect.setText(2131099668); break label130; } Enumeration
		 * localEnumeration2 =
		 * ((NetworkInterface)localEnumeration1.nextElement()
		 * ).getInetAddresses(); while (localEnumeration2.hasMoreElements()) {
		 * InetAddress localInetAddress =
		 * (InetAddress)localEnumeration2.nextElement(); if
		 * ((localInetAddress.isLoopbackAddress()) ||
		 * (!InetAddressUtils.isIPv4Address(localInetAddress.getHostAddress())))
		 * continue; ipaddress = localInetAddress.getHostAddress();
		 * System.out.println("Print Ipaddress:- " + ipaddress); } } } catch
		 * (SocketException localSocketException) { Log.d("sss",
		 * localSocketException.toString()); } }
		 */
	}

	public void printLog(String paramString) {
		Log.d("sss", paramString);
	}

	void restartVPN(String paramString) {
		Intent localIntent = new Intent("doenter.onevpn.ACTION_CONNECT");
		localIntent.putExtra("name", paramString);
		localIntent.putExtra("force", true);
		localIntent.putExtra("force_same", true);
		startActivity(localIntent);
	}

	public void show_Connect_VPN_dialog(HashMap<String, String> paramHashMap) {
		this.myDialog = new Dialog(this.context);
		this.myDialog.requestWindowFeature(1);
		this.myDialog.setContentView(R.layout.dialog_connect);
		this.myDialog.getWindow().setLayout(-2, -2);
		this.myDialog.setCancelable(true);
		Button localButton1 = (Button) this.myDialog
				.findViewById(R.id.btn_Connect);
		Button localButton2 = (Button) this.myDialog
				.findViewById(R.id.btn_Cancel);
		((EditText) this.myDialog.findViewById(R.id.et_User_Name))
				.setText((CharSequence) paramHashMap.get("Username"));
		((EditText) this.myDialog.findViewById(R.id.et_Password))
				.setText((CharSequence) paramHashMap.get("Password"));
		((EditText) this.myDialog.findViewById(R.id.et_Proxy_Server))
				.setText((CharSequence) paramHashMap.get("Proxy"));
		((EditText) this.myDialog.findViewById(R.id.et_Port))
				.setText((CharSequence) paramHashMap.get("ProxyPort"));
		localButton1.setOnClickListener(new View.OnClickListener() {
			@SuppressLint({ "NewApi" })
			public void onClick(View paramView) {
				/*
				 * Intent localIntent =
				 * VpnService.prepare(MainActivity.this.context); if
				 * (localIntent != null)
				 * MainActivity_VPN.this.startActivityForResult(localIntent, 1);
				 * while (true) { if
				 * (MainActivity_VPN.this.myDialog.isShowing())
				 * MainActivity_VPN.this.myDialog.dismiss(); return;
				 * MainActivity_VPN.this.onActivityResult(1, -1, null); }
				 */
			}
		});
		localButton2.setOnClickListener(new View.OnClickListener() {
			public void onClick(View paramView) {
				MainActivity.this.myDialog.dismiss();
			}
		});
		this.myDialog.show();
	}

	void startVPN(String paramString) {
		startActivityForResult(
				new Intent("com.android.settings.vpn2.VpnDialog"), 1000);
	}

	void stopVPN() {
		startActivity(new Intent("doenter.onevpn.ACTION_DISCONNECT"));
	}

	@Override
	protected void onResume() {
		super.onResume();
		
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			if(requestCode == 101){
			Intent intent = new Intent(this, MyVpnService.class);
			startService(intent);
			}
		}
	}
	
	
}
