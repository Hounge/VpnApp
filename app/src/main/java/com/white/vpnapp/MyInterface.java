package com.white.vpnapp;

public abstract interface MyInterface
{
  public static final String BASE_URL = "http://74.54.90.136/login_webservice/";
  public static final String LOG_TAG = "sss";
  public static final String PACKAGE_NAME = "com.white.vpnapp";
  public static final String TAG_AccountNumber = "AccountNumber";
  public static final String TAG_EmailAddress = "EmailAddress";
  public static final String TAG_IPAddress = "IPAddress";
  public static final String TAG_LoginDetails = "LoginDetails";
  public static final String TAG_Paid = "Paid";
  public static final String TAG_Password = "Password";
  public static final String TAG_Proxy = "Proxy";
  public static final String TAG_ProxyPort = "ProxyPort";
  public static final String TAG_Trial = "Trial";
  public static final String TAG_Username = "Username";
  public static final String TAG_data = "data";
  public static final String TAG_msg = "msg";
  public static final String TAG_status = "status";
  public static final String TAG_strHashMap = "strhashmap";
  public static final String TAG_success = "success";
  public static final String TAG_user_id = "user_id";
  public static final String TAG_userid = "userid";
  public static final String URL_LOGIN = "http://74.54.90.136/login_webservice/login.php";
  public static final String URL_MOBILE_CONFIG = "http://74.54.90.136/login_webservice/salman.mobileconfig";
  public static final String URL_REGISTER = "http://74.54.90.136/login_webservice/registration.php";
  public static final String URL_SUBSCRIBE = "http://74.54.90.136/auth_net/authorize_net.php?";

  public abstract void printLog(String paramString);
}

/* Location:           C:\apktool\classes_dex2jar.jar
 * Qualified Name:     com.vijaywebsolutions.services.MyInterface
 * JD-Core Version:    0.6.0
 */