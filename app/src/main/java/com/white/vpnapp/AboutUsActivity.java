package com.white.vpnapp;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.os.Build;

public class AboutUsActivity extends Activity implements OnClickListener {

	Context context;
	  ImageView iv_Back;
	  TextView tv_About_Us;
	  TextView tv_Title;

	  private void init_var()
	  {
	    this.context = this;
	    this.tv_Title = ((TextView)findViewById(R.id.tv_Title));
	    this.tv_About_Us = ((TextView)findViewById(R.id.tv_About_Us));
	    this.tv_Title.setText(this.context.getResources().getString(R.string.title_activity_about_us));
	    SpannableString localSpannableString = new SpannableString(this.tv_About_Us.getText().toString().trim());
	    localSpannableString.setSpan(new StyleSpan(1), 0, 17, 0);
	    localSpannableString.setSpan(new StyleSpan(1), 435, 451, 0);
	    this.tv_About_Us.setText(localSpannableString);
	    this.iv_Back = ((ImageView)findViewById(R.id.iv_Back));
	    this.iv_Back.setOnClickListener(this);
	  }

	  public void onClick(View paramView)
	  {
	    switch (paramView.getId())
	    {
	    default:
	      return;
	    case R.id.iv_Back:
	    	onBackPressed();
	    	finish();
	    	break;
	    }
	    
	  }

	  protected void onCreate(Bundle paramBundle)
	  {
	    super.onCreate(paramBundle);
	    setContentView(R.layout.about_us_activity);
	    init_var();
	  }



}
