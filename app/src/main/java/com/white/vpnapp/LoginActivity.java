package com.white.vpnapp;

import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends Activity implements OnClickListener, MyInterface{

	Button btn_Login;
	  Context context;
	  EditText et_Password;
	  EditText et_User_Name;
	  HashMap<String, String> hashMap;
	  private HashMap<String, String> hashMap_ServerDetails;
	  MyClass myClass;
	  SessionData sessionData;
	  TextView tv_Sign_Up;
	  TextView tv_Title;

	  private void init_var()
	  {
	    this.myClass = new MyClass(this.context);
	    this.btn_Login = ((Button)findViewById(R.id.btn_Login));
	    this.btn_Login.setOnClickListener(this);
	    this.tv_Sign_Up = ((TextView)findViewById(R.id.tv_Sign_Up));
	    this.tv_Sign_Up.setOnClickListener(this);
	    this.et_User_Name = ((EditText)findViewById(R.id.et_User_Name));
	    this.et_Password = ((EditText)findViewById(R.id.et_Password));
	  }

	  public void onClick(View paramView)
	  {
	    switch (paramView.getId())
	    {
	    default:
	      return;
	    case R.id.iv_Back:
	      onBackPressed();
	    case R.id.tv_Sign_Up:
	      startActivity(new Intent(this.context, RegisterActivity.class));
	      return;
	    case R.id.btn_Login:
	    
	    if (this.et_User_Name.getText().toString().trim().length() == 0)
	    {
	      this.myClass.setErrorMessage(this.et_User_Name, this.context.getResources().getString(2131099688));
	      return;
	    }
	    if (this.et_Password.getText().toString().trim().length() == 0)
	    {
	      this.myClass.setErrorMessage(this.et_Password, this.context.getResources().getString(2131099690));
	      return;
	    }
	    this.hashMap = new HashMap();
	    this.hashMap.put("Username", this.et_User_Name.getText().toString().trim());
	    this.hashMap.put("Password", this.et_Password.getText().toString().trim());
	    new AsyncTaskUtility(this.context, this.hashMap, "http://74.54.90.136/login_webservice/login.php").getJSONResponse(new AsyncTaskInterface()
	    {
	      public void getJSONObjectFromAsynTask(JSONObject paramJSONObject)
	      {
	        try
	        {
	          JSONObject localJSONObject = paramJSONObject.getJSONObject("data");
	          hashMap_ServerDetails = new HashMap();
	          hashMap_ServerDetails.put("Proxy", localJSONObject.getString("Proxy"));
	          hashMap_ServerDetails.put("Paid", localJSONObject.getString("Paid"));
	          hashMap_ServerDetails.put("Trial", localJSONObject.getString("Trial"));
	          hashMap_ServerDetails.put("IPAddress", localJSONObject.getString("IPAddress"));
	          hashMap_ServerDetails.put("AccountNumber", localJSONObject.getString("AccountNumber"));
	          hashMap_ServerDetails.put("Password", localJSONObject.getString("Password"));
	          hashMap_ServerDetails.put("Username", localJSONObject.getString("Username"));
	          hashMap_ServerDetails.put("ProxyPort", localJSONObject.getString("ProxyPort"));
	          if (paramJSONObject.getString("status").equalsIgnoreCase("success"))
	          {
	            startActivity(new Intent(context, MainActivity.class));
	            sessionData.setString("strhashmap", new Gson().toJson(hashMap_ServerDetails));
	            finish();
	          }
	          return;
	        }
	        catch (JSONException localJSONException)
	        {
	          localJSONException.printStackTrace();
	        }
	      }
	    });
	    }
	  }
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		 this.context = this;
		    this.sessionData = new SessionData(this.context, "com.white.vpnapp");
		    if (this.sessionData.getString("strhashmap") != null)
		    {
		      startActivity(new Intent(this.context, MainActivity.class));
		      finish();
		      return;
		    }
		    init_var();
	}
	 public void printLog(String paramString)
	  {
	    Log.d("sss", paramString);
	  }

	  public class Login_TaskAction extends AsyncTask<Void, Void, Void>
	  {
	    HashMap<String, String> hashMap1;
	    JSONObject json;
	    String strURL;

	    public Login_TaskAction(String url,HashMap<String, String> arg2)
	    {
	      this.strURL = url;	      
	      this.hashMap1 = arg2;
	    }

	    protected Void doInBackground(Void[] paramArrayOfVoid)
	    {
	      JSONParser localJSONParser = new JSONParser();
	      printLog("PARAMETERS: " + this.hashMap1.toString());
	      this.json = localJSONParser.getJsonObjectFromWebservice(this.strURL, this.hashMap1);
	      return null;
	    }

	    protected void onPostExecute(Void paramVoid)
	    {
	      super.onPostExecute(paramVoid);
	      myClass.dismissProgressDialog();
	      try
	      {
	        printLog("JSON: " + this.json);
	        if (this.json != null)
	        {
	          if (this.json.has("msg"))
	          {
	            String str = this.json.getString("msg");
	            if (str.toString().trim().length() != 0)
	            {
	              myClass.showToast(str);
	              return;
	            }
	          }
	        }
	        else
	        {
	          myClass.showToast("");
	          return;
	        }
	      }
	      catch (JSONException localJSONException)
	      {
	        localJSONException.printStackTrace();
	      }
	    }

	    protected void onPreExecute()
	    {
	      super.onPreExecute();
	      myClass.showProgressDialog();
	    }
	  }

	
	
	

}
