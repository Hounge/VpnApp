package com.white.vpnapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.util.Log;
import android.view.ContextThemeWrapper;
import java.util.HashMap;
import org.json.JSONObject;

public class AsyncTaskUtility
  implements MyInterface
{
  private AsyncTaskInterface asyncTaskInterface;
  private Context context;
  private HashMap<String, String> map;
  MyClass myClass;
  private String strURL;

  public AsyncTaskUtility(Context paramContext, HashMap<String, String> paramHashMap, String paramString)
  {
    this.context = paramContext;
    this.map = paramHashMap;
    this.strURL = paramString;
    this.myClass = new MyClass(this.context);
  }

  public void getJSONResponse(AsyncTaskInterface paramAsyncTaskInterface)
  {
    this.asyncTaskInterface = paramAsyncTaskInterface;
    if (this.myClass.isInternetAvailable())
      new AsyncTaskAction().execute(new String[0]);
  }

  public void printLog(String paramString)
  {
    Log.d("sss", paramString);
  }

  public class AsyncTaskAction extends AsyncTask<String, Void, Void>
  {
    private JSONObject json;
    private ProgressDialog progressBar;
    private String status = "";

    public AsyncTaskAction()
    {
    }

    protected Void doInBackground(String[] paramArrayOfString)
    {
      try
      {
        this.json = new JSONParser().getJsonObjectFromWebservice(AsyncTaskUtility.this.strURL, AsyncTaskUtility.this.map);
        return null;
      }
      catch (Exception localException)
      {
        while (true)
        {
          localException.printStackTrace();
          AsyncTaskUtility.this.myClass.showToast("Error Please try later.");
        }
      }
    }

    protected void onPostExecute(Void paramVoid)
    {
      super.onPostExecute(paramVoid);
      this.progressBar.dismiss();
      AsyncTaskUtility.this.printLog("JSON: " + this.json);
      try
      {
        if (this.json != null)
        {
          if (this.json.has("msg"))
          {
            String str = this.json.getString("msg");
            if (str.toString().trim().length() != 0)
              AsyncTaskUtility.this.myClass.showToast(str);
          }
          this.status = this.json.getString("status");
        }
        if (this.status.equalsIgnoreCase("Success"))
        	AsyncTaskUtility.this.asyncTaskInterface.getJSONObjectFromAsynTask(this.json);
        else
        	AsyncTaskUtility.this.myClass.showToast(AsyncTaskUtility.this.context.getResources().getString(2131099680));
      }
      catch (Exception localException)
      {
    	  localException.printStackTrace();
    	  AsyncTaskUtility.this.myClass.showToast("Error Please try later.");
      }
    }

    protected void onPreExecute()
    {
      super.onPreExecute();
      this.progressBar = new ProgressDialog(new ContextThemeWrapper(AsyncTaskUtility.this.context, 2131165184));
      this.progressBar.setCancelable(false);
      this.progressBar.setMessage("Loading please wait");
      this.progressBar.setProgressStyle(0);
      this.progressBar.show();
    }
  }
}

/* Location:           C:\apktool\classes_dex2jar.jar
 * Qualified Name:     com.vijaywebsolutions.services.AsyncTaskUtility
 * JD-Core Version:    0.6.0
 */